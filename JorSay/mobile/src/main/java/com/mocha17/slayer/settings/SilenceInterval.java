package com.mocha17.slayer.settings;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Chaitanya on 4/23/16.
 */
public class SilenceInterval implements Comparable<SilenceInterval> {
    public static final Type TYPE;

    public long startMilli;
    public long endMilli;

    static {
        TYPE = new TypeToken<List<SilenceInterval>>() {}.getType();
    }

    @Override
    public int compareTo(SilenceInterval another) {
        if (another == null) {
            return 1;
        }
        if (startMilli < another.startMilli) {
            return -1;
        }
        if (startMilli > another.startMilli) {
            return 1;
        }
        if (startMilli == another.startMilli) {
            if (endMilli < another.endMilli) {
                return -1;
            }
            if (endMilli > another.endMilli) {
                return 1;
            }
            if (endMilli == another.endMilli) {
                return 0;
            }
        }
        return 0;
    }
}
