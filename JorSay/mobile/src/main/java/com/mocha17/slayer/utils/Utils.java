package com.mocha17.slayer.utils;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.text.format.DateFormat;

import com.mocha17.slayer.SlayerApp;
import com.mocha17.slayer.settings.SilenceInterval;

import java.util.Calendar;

/**
 * Created by mocha on 7/2/15.
 */
public class Utils {
    private static final java.text.DateFormat formatter;
    static {
        formatter = DateFormat.getTimeFormat(SlayerApp.getInstance());
    }

    /**
     * @param packageName - an application package name
     * @return user-readable application name if found, incoming package name otherwise */
    public static String getAppName(String packageName) {
        if (TextUtils.isEmpty(packageName)) {
            return packageName;
        }
        PackageManager packageManager = SlayerApp.getInstance().getPackageManager();
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(
                    packageName, PackageManager.COMPONENT_ENABLED_STATE_DEFAULT);
            return applicationInfo.loadLabel(packageManager).toString();
        } catch (PackageManager.NameNotFoundException e) {
            Logger.e(Utils.class, "getAppName NameNotFound for " + packageName + ", returning it");
            return packageName;
        }
    }

    /** Returns a String with emoji characters removed from the input String */
    public static String withoutEmoji(String s) {
        if (TextUtils.isEmpty(s)) {
            return s;
        }
        //From http://stackoverflow.com/a/12014635/299988
        //We could also go for [\\p{InMiscellaneousSymbolsAndPictographs}|\\p{InEmoticons}]+ but
        //it does not match the '❤' symbol and might not match others
        return s.replaceAll("\\p{So}+", "");
    }

    public static long getDayStartMilli() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTimeInMillis();
    }

    /**
     * @param dayStartMilli Unix time for start of the day
     * @return a String representation of silence interval
     */
    public static String getIntervalString(long dayStartMilli, SilenceInterval silenceInterval) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(silenceInterval.startMilli + dayStartMilli);
        StringBuilder stringBuilder = new StringBuilder(formatter.format(calendar.getTime()));
        calendar.setTimeInMillis(silenceInterval.endMilli + dayStartMilli);
        stringBuilder.append(" - ").append(formatter.format(calendar.getTime()));
        return stringBuilder.toString();
    }

    public static String getTimeString(Calendar calendar) {
        if (calendar == null) {
            return "";
        }
        return formatter.format(calendar.getTime());
    }
}
