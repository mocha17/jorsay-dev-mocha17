package com.mocha17.slayer.settings;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mocha17.slayer.R;
import com.mocha17.slayer.utils.Logger;
import com.mocha17.slayer.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * Created by Chaitanya on 4/13/16.
 */
public class SelectSilenceIntervalsDialog extends DialogFragment implements View.OnClickListener {
    private static final String ACTION_TIME_SET = "ACTION_TIME_SET";
    private static final String KEY_HOUR_OF_DAY = "KEY_HOUR_OF_DAY";
    private static final String KEY_MINUTE = "KEY_MINUTE";
    private static final String KEY_ADD_INTERVAL_TIME_SET = "KEY_ADD_INTERVAL_TIME_SET";
    private static final String KEY_CURRENT_INTERVAL = "KEY_CURRENT_INTERVAL";

    private Context context;
    private SharedPreferences defaultSharedPreferences;
    private SharedPreferences.Editor editor;
    private Gson gson;

    private CheckedTextView allTimes;
    private View silenceIntervalsContainer;

    private long dayStartMilli;
    private List<SilenceInterval> selectedSilenceIntervals;
    private SilenceInterval currentSilenceInterval;
    private ListView silenceIntervalsList;
    private ArrayAdapter silenceIntervalsListAdapter;

    private TimePickerFragment addIntervalTimePicker;
    private boolean addIntervalStartTimeSet;

    static SelectSilenceIntervalsDialog newInstance() {
        return new SelectSilenceIntervalsDialog();
    }

    //public Constructor
    public SelectSilenceIntervalsDialog() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        context = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = defaultSharedPreferences.edit();
        gson = new Gson();
        dayStartMilli = Utils.getDayStartMilli();

        //For timeSet
        LocalBroadcastManager.getInstance(context).
                registerReceiver(timeSetBroadcastReceiver, new IntentFilter(ACTION_TIME_SET));
    }

    @Override
    public void onDestroy() {
        if (timeSetBroadcastReceiver != null) {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(timeSetBroadcastReceiver);
        }
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_select_silence_intervals, container, false);

        Button ok = (Button)view.findViewById(R.id.ok_button);
        ok.setOnClickListener(this);

        Button cancel = (Button)view.findViewById(R.id.cancel_button);
        cancel.setOnClickListener(this);

        allTimes = (CheckedTextView)view.findViewById(R.id.all_times);
        allTimes.setOnClickListener(this);

        silenceIntervalsContainer = view.findViewById(R.id.silence_intervals_container);

        Button addInterval = (Button) view.findViewById(R.id.add_interval_button);
        addInterval.setOnClickListener(this);

        silenceIntervalsList = (ListView) view.findViewById(R.id.silence_intervals_list);

        //Update UI based on savedInstanceState (if available) and the info in SharedPreferences

        //Get previously selected intervals
        String prevSelectedStr = defaultSharedPreferences.getString(
                getString(R.string.pref_key_silence_intervals), "");
        List<SilenceInterval> prevSelected = gson.fromJson(prevSelectedStr, SilenceInterval.TYPE);

        if (savedInstanceState != null) {
            if (savedInstanceState.getBoolean(getString(R.string.pref_key_all_times))) {
                //All times are selected by the user, populate selectedSilenceIntervals from SharedPreferences
                if (prevSelected != null) {
                    selectedSilenceIntervals = new ArrayList<>(prevSelected);
                } else {
                    selectedSilenceIntervals = new ArrayList<>();
                }
                updateUI(true/*allTimesChecked*/);
            } else {
                //User has potentially selected some intervals. Restore the UI appropriately.
                String tempSelectedStr = savedInstanceState.getString(
                        getString(R.string.pref_key_silence_intervals), "");
                List<SilenceInterval> tempSelected = gson.fromJson(tempSelectedStr, SilenceInterval.TYPE);
                if (tempSelected != null) {
                    selectedSilenceIntervals = new ArrayList<>(tempSelected);
                } else {
                    //We do not have saved selected intervals, use the list from sharedPreferences
                    if (prevSelected != null) {
                        selectedSilenceIntervals = new ArrayList<>(prevSelected);
                    } else {
                        selectedSilenceIntervals = new ArrayList<>();
                    }
                }
                updateUI(false/*allTimesChecked*/);
            }
            addIntervalStartTimeSet = savedInstanceState.getBoolean(KEY_ADD_INTERVAL_TIME_SET);
            currentSilenceInterval = gson.fromJson(
                    savedInstanceState.getString(KEY_CURRENT_INTERVAL), SilenceInterval.class);
        } else { //When we don't have a saved state, set UI state based on SharedPreferences data
            if (prevSelected != null) {
                selectedSilenceIntervals = new ArrayList<>(prevSelected);
            } else {
                selectedSilenceIntervals = new ArrayList<>();
            }
            updateUI(defaultSharedPreferences.getBoolean(
                    getString(R.string.pref_key_all_times), false));
        }
        return view;
    }

    @Override
    public void onSaveInstanceState (Bundle outState) {
        //Store if 'all intervals' is selected
        outState.putBoolean(getString(R.string.pref_key_all_times), allTimes.isChecked());
        if (!allTimes.isChecked()) {
            if (selectedSilenceIntervals != null && !selectedSilenceIntervals.isEmpty()) {
                //Store currently selected intervals
                outState.putString(getString(R.string.pref_key_silence_intervals),
                        gson.toJson(selectedSilenceIntervals));
            }
        }
        outState.putBoolean(KEY_ADD_INTERVAL_TIME_SET, addIntervalStartTimeSet);
        if (currentSilenceInterval != null) {
            outState.putString(KEY_CURRENT_INTERVAL, gson.toJson(currentSilenceInterval));
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_interval_button:
                addIntervalStartTimeSet = false; //Always 'start' dialog on tapping this button
                showAddIntervalTimePicker(true /*addIntervalStart*/);
                break;
            case R.id.ok_button:
                editor.putString(getString(R.string.pref_key_silence_intervals),
                        gson.toJson(selectedSilenceIntervals)).apply();
                if (selectedSilenceIntervals.isEmpty() && !allTimes.isChecked()) {
                    editor.putBoolean(getString(R.string.pref_key_all_times), true).apply();
                    Toast.makeText(context, R.string.no_selected_intervals, Toast.LENGTH_LONG).show();
                } else {
                    editor.putBoolean(
                            getString(R.string.pref_key_all_times), allTimes.isChecked()).apply();
                }
                /*fall through*/
            case R.id.cancel_button:
                getDialog().dismiss();
                break;
            case R.id.all_times:
                allTimes.toggle();
                updateUI(allTimes.isChecked());
                break;
        }
    }

    /** Insert currentSilenceInterval into selectedSilenceIntervals, keep selectedSilenceIntervals sorted
     * by start time and merge currentSilenceInterval with an already existing interval if appropriate.
     * Also update the UI.*/
    private void handleInsert() {
        boolean merged = false;
        for (int i=0; i<selectedSilenceIntervals.size(); i++) {
            /* Suppose selectedSilenceIntervals.get(i) is 9 AM - 10 AM.
            Merge currentSilenceInterval with when it is -
                 1. 9:15 AM - 9:45 AM - nothing to be done here
                 2. 8 AM - 9:30 AM - new silenceInterval is 8 AM - 10 AM
                 3. 9:30 AM - 10:30 AM - new silenceInterval is 9 AM - 10:30 AM
                 4. 8:30 AM - 10:30 AM - new silenceInterval is 8:30 AM - 10:30 AM */
            SilenceInterval silenceInterval = selectedSilenceIntervals.get(i);
            if (currentSilenceInterval.startMilli >= silenceInterval.startMilli &&
                    currentSilenceInterval.endMilli <= silenceInterval.endMilli) {
                Toast.makeText(context, context.getString(R.string.add_interval_merge_both_times_1,
                        Utils.getIntervalString(dayStartMilli, currentSilenceInterval),
                        Utils.getIntervalString(dayStartMilli, silenceInterval)),
                        Toast.LENGTH_LONG).show();
                return;
            } else if (currentSilenceInterval.startMilli <= silenceInterval.startMilli &&
                    (currentSilenceInterval.endMilli >= silenceInterval.startMilli &&
                            currentSilenceInterval.endMilli <= silenceInterval.endMilli)) {
                Toast.makeText(context, context.getString(R.string.add_interval_merge_one_time,
                        Utils.getIntervalString(dayStartMilli, currentSilenceInterval),
                        Utils.getIntervalString(dayStartMilli, silenceInterval)),
                        Toast.LENGTH_LONG).show();
                silenceInterval.startMilli = currentSilenceInterval.startMilli;
                selectedSilenceIntervals.set(i, silenceInterval);
                merged = true;
                break;
            } else if ((currentSilenceInterval.endMilli >= silenceInterval.endMilli) &&
                    (currentSilenceInterval.startMilli >= silenceInterval.startMilli &&
                            currentSilenceInterval.startMilli <= silenceInterval.endMilli)) {
                Toast.makeText(context, context.getString(R.string.add_interval_merge_one_time,
                        Utils.getIntervalString(dayStartMilli, currentSilenceInterval),
                        Utils.getIntervalString(dayStartMilli, silenceInterval)),
                        Toast.LENGTH_LONG).show();
                silenceInterval.endMilli = currentSilenceInterval.endMilli;
                selectedSilenceIntervals.set(i, silenceInterval);
                merged = true;
                break;
            } else if (currentSilenceInterval.startMilli <= silenceInterval.startMilli &&
                    currentSilenceInterval.endMilli >= silenceInterval.endMilli) {
                selectedSilenceIntervals.set(i, currentSilenceInterval);
                Toast.makeText(context, context.getString(R.string.add_interval_merge_both_times_2,
                        Utils.getIntervalString(dayStartMilli, currentSilenceInterval),
                        Utils.getIntervalString(dayStartMilli, silenceInterval)),
                        Toast.LENGTH_LONG).show();
                merged = true;
            }
        }
        if (!merged) {
            selectedSilenceIntervals.add(currentSilenceInterval);
        }

        Collections.sort(selectedSilenceIntervals);

        silenceIntervalsListAdapter.notifyDataSetChanged();
    }

    private void showAddIntervalTimePicker(boolean addIntervalStart) {
        String addIntervalTitle = addIntervalStart?
                getString(R.string.interval_start_title):
                getString(R.string.interval_end_title);

        if (addIntervalTimePicker != null) {
            /* This 'if' block is our attempt at taking care of DialogFragment lifecycle. We don't
            expect the dialog to be visible at this point, however, we dismiss it if it is visible.
            We also execute any pending transactions and set the instance to null before instantiating it.*/
            if (addIntervalTimePicker.isVisible()) {
                addIntervalTimePicker.dismiss();
            }
            getFragmentManager().executePendingTransactions();
            addIntervalTimePicker = null;
        }
        addIntervalTimePicker = new TimePickerFragment();
        Bundle args = new Bundle();
        args.putString(TimePickerFragment.KEY_TITLE, addIntervalTitle);
        addIntervalTimePicker.setArguments(args);
        addIntervalTimePicker.show(getFragmentManager(),
                TimePickerFragment.class.getSimpleName().concat(addIntervalTitle));
    }

    private void updateUI(boolean allTimesChecked) {
        if (allTimesChecked) {
            allTimes.setChecked(true);
            silenceIntervalsContainer.setVisibility(View.INVISIBLE);
        } else {
            silenceIntervalsContainer.setVisibility(View.VISIBLE);
            if (silenceIntervalsListAdapter == null) {
                silenceIntervalsListAdapter = new SilenceIntervalsAdapter();
            }
            silenceIntervalsList.setAdapter(silenceIntervalsListAdapter);
        }
    }

    private class SilenceIntervalsAdapter extends ArrayAdapter<SilenceInterval> {
        private SilenceIntervalsAdapter() {
            super(context, R.layout.row_silence_intervals_list, selectedSilenceIntervals);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (selectedSilenceIntervals == null || selectedSilenceIntervals.size() <= position) {
                return convertView;
            }
            if (convertView == null) {
                convertView =LayoutInflater.from(context).inflate(
                        R.layout.row_silence_intervals_list, parent, false);
            }
            TextView interval = (TextView) convertView.findViewById(R.id.selected_interval_text);
            interval.setText(Utils.getIntervalString(dayStartMilli,
                    selectedSilenceIntervals.get(position)));
            ImageButton remove = (ImageButton) convertView.findViewById(R.id.selected_interval_remove);
            remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SilenceInterval removed = selectedSilenceIntervals.remove(position);
                    Logger.d(this, "Removed " + Utils.getIntervalString(dayStartMilli, removed));
                    Collections.sort(selectedSilenceIntervals);
                    notifyDataSetChanged();
                }
            });
            return convertView;
        }
    }

    private BroadcastReceiver timeSetBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_TIME_SET.equals(intent.getAction())) {
                int hourOfDay = intent.getIntExtra(KEY_HOUR_OF_DAY, 0);
                int minute = intent.getIntExtra(KEY_MINUTE, 0);

                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                long diffMilli = calendar.getTimeInMillis() - dayStartMilli;
                if (!addIntervalStartTimeSet) { //interval start
                    if (currentSilenceInterval == null) {
                        currentSilenceInterval = new SilenceInterval();
                    }
                    currentSilenceInterval.startMilli  = diffMilli;
                    showAddIntervalTimePicker(false/*addIntervalStart*/);
                    addIntervalStartTimeSet = true;
                } else { //interval end
                    if (diffMilli <= currentSilenceInterval.startMilli) {
                        Toast.makeText(context, R.string.add_interval_error, Toast.LENGTH_SHORT).show();
                        showAddIntervalTimePicker(false/*addIntervalStart*/);
                    } else {
                        currentSilenceInterval.endMilli = diffMilli;
                        handleInsert();
                    }
                }
            }
        }
    };
    /* The inner Fragment class needs to be public static so that it is independent of the
    Activity lifecycle and doesn't result in memory leaks. We need to access enclosing class members
    onTimeSet to do anything useful, and we use LocalBroadcasts for it.*/
    public static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{
        static final String KEY_TITLE = "KEY_TITLE";
        private Context context;

        public TimePickerFragment() {
            //empty Constructor
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            context = activity;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final String title = getArguments().getString(KEY_TITLE);
            final Calendar calendar = Calendar.getInstance();
            // Create and return a new instance of TimePickerDialog with current time as default values
            TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), this,
                    calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),
                    DateFormat.is24HourFormat(getActivity()));
            if (!TextUtils.isEmpty(title)) {
                timePickerDialog.setTitle(title);
            }
            return timePickerDialog;
        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Intent intent = new Intent(ACTION_TIME_SET);
            intent.putExtra(KEY_HOUR_OF_DAY, hourOfDay);
            intent.putExtra(KEY_MINUTE, minute);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }
    }
}